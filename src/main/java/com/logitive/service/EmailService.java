package com.logitive.service;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.app.event.implement.IncludeRelativePath;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.logitive.LookeriaUtilities;
import com.logitive.commons.email.EmailAttributesFactory;
import com.logitive.commons.email.EmailContext;
import com.logitive.commons.email.EmailDataWrapper;
import com.logitive.commons.email.MailQueueStatus;
import com.logitive.domain.Email;
import com.logitive.domain.EmailAttachment;
import com.logitive.domain.EmailContent;
import com.logitive.repository.EmailAttachmentRepository;
import com.logitive.repository.EmailContentRepository;
import com.logitive.repository.EmailRepository;
import com.logitive.repository.UserRepository;
import com.logitive.service.interfaces.IEmailService;

@Service
public class EmailService implements IEmailService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(EmailService.class.getName());

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private EmailAttributesFactory emailAttributesFactory;
    @Autowired
    private EmailContentRepository emailContentRepository;
    @Autowired
    private EmailAttachmentRepository emailAttachmentRepository;
    @Autowired
    private EmailRepository emailRepository;
    @Autowired
    private UserRepository userRepository;

    @Async
    @Override
    public void saveAndSendBulkEmailsAsync(List<EmailDataWrapper> dataWrappers) {
        saveAndSendBulkEmails(dataWrappers);
    }

    @Async
    @Override
    public void saveAndSendEmailAsync(EmailDataWrapper dataWrapper) {
        saveAndSendBulkEmailsAsync(Arrays
                .asList(new EmailDataWrapper[] { dataWrapper }));
    }

    @Override
    public void saveAndSendBulkEmails(List<EmailDataWrapper> dataWrappers) {
        Map<MimeMessage, EmailDataWrapper> mimeEmailMap = new HashMap<>();
        for (EmailDataWrapper emailSession : dataWrappers) {
            saveEmailData(emailSession);
            MimeMessage mimeMessage = compose(emailSession);
            mimeEmailMap.put(mimeMessage, emailSession);
        }
        sendAndUpdate(mimeEmailMap);
    }

    @Override
    public void saveAndSendEmail(EmailDataWrapper dataWrapper) {
        saveAndSendBulkEmails(Arrays
                .asList(new EmailDataWrapper[] { dataWrapper }));
    }

    private EmailDataWrapper saveEmailData(EmailDataWrapper dataWrapper) {
        Email email = dataWrapper.getEmail();
        List<EmailAttachment> attachments = dataWrapper.getAttachments();
        email = emailRepository.save(email);
        EmailContent emailContent;
        if (dataWrapper.getEmailContent() != null) {
            emailContent = dataWrapper.getEmailContent();
            emailContent.setEmailId(email.getId());
            emailContent = emailContentRepository.save(emailContent);
        } else {
            Map<String, Object> emailAttributes = emailAttributesFactory
                    .getAttributes(dataWrapper);
            if (email.getEmailContentId() != null) {
                emailContent = emailContentRepository.findOne(email
                        .getEmailContentId());
            } else {
                emailContent = getEmailContent(dataWrapper, emailAttributes);
            }
            dataWrapper.setEmailContent(emailContent);
        }
        email.setEmailContentId(emailContent.getId());
        email = emailRepository.save(email);
        setAttachments(email, attachments);
        dataWrapper.setEmail(email);
        return dataWrapper;
    }

    private MimeMessage compose(EmailDataWrapper dataWrapper) {
        Email email = dataWrapper.getEmail();
        EmailContent emailContent = dataWrapper.getEmailContent();
        List<EmailAttachment> attachments = dataWrapper.getAttachments();
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,
            // attachments != null && !attachments.isEmpty()
                    true);
            prepareMessageBody(emailContent, helper);
            prepareHeader(email, emailContent, helper);
            prepareAttachment(attachments, helper);
            helper.setSentDate(Calendar
                    .getInstance(TimeZone.getTimeZone("GMT")).getTime());
        } catch (MessagingException | UnsupportedEncodingException e) {
            updateMailStatus(email, MailQueueStatus.INVALID.toString());
            LOGGER.error(e.getMessage(), e);
        }
        return mimeMessage;
    }

    private EmailContent getEmailContent(EmailDataWrapper dataWrapper,
            Map<String, Object> emailAttributes) {
        EmailContent emailContent = new EmailContent();
        EmailContext context = dataWrapper.getContext();
        emailContent.setEmailContext(context.getName());
        emailContent.setEmailSubject(context.getSubject());
        emailContent.setEmailBody(getFormattedString(context.getTemplate(),
                getVelocityContext(emailAttributes)));
        emailContent.setEmailId(dataWrapper.getEmail().getId());
        return emailContentRepository.save(emailContent);
    }

    private void setAttachments(Email email, List<EmailAttachment> attachments) {
        if (attachments != null) {
            if (email.getEmailAttachmentIds() == null) {
                email.setEmailAttachmentIds(new ArrayList<String>());
            }
            for (EmailAttachment attachment : attachments) {
                attachment.setEmailId(email.getId());
                attachment = emailAttachmentRepository.save(attachment);
                email.getEmailAttachmentIds().add(attachment.getId());
            }
            emailRepository.save(email);
        }
    }

    private void sendAndUpdate(Map<MimeMessage, EmailDataWrapper> mimeEmailMap) {
        System.setProperty("mail.mime.multipart.allowempty", "true");
        Timestamp timestamp = LookeriaUtilities.getTimestamp();
        List<Email> emails = new ArrayList<>();
        for (EmailDataWrapper dataWrapper : mimeEmailMap.values()) {
            Email email = dataWrapper.getEmail();
            email.setLastTriedAt(timestamp);
            if (!email.getMailStatus().equals(
                    MailQueueStatus.TO_BE_SENT.toString())) {
                email.setRetries(email.getRetries() + 1);
            }
            emails.add(email);
        }
        updateMailStatus(emails, MailQueueStatus.PROGRESS.toString());
        sendEmails(mimeEmailMap, emails);
    }

    private void sendEmails(Map<MimeMessage, EmailDataWrapper> mimeEmailMap,
            List<Email> emails) {
        try {
            mailSender.send(mimeEmailMap.keySet().toArray(new MimeMessage[0]));
            Timestamp timestamp = LookeriaUtilities.getTimestamp();
            for (EmailDataWrapper emailSession : mimeEmailMap.values()) {
                Email email = emailSession.getEmail();
                email.setSentAt(timestamp);
                updateMailStatus(email, MailQueueStatus.SENT.toString());
            }
            emailRepository.save(emails);
        } catch (MailAuthenticationException mae) {
            updateMailStatus(emails, MailQueueStatus.FAILED.toString());
            LOGGER.error(mae.getMessage(), mae);
        } catch (MailSendException mse) {
            Set<Object> failedMessages = mse.getFailedMessages().keySet();
            List<Email> failedEmails = new ArrayList<>();
            for (Object message : failedMessages) {
                EmailDataWrapper session = mimeEmailMap
                        .get((MimeMessage) message);
                Email email = session.getEmail();
                failedEmails.add(email);
            }
            updateMailStatus(failedEmails, MailQueueStatus.RETRY.toString());
            LOGGER.error(mse.getMessage(), mse);
        } catch (MailException me) {
            updateMailStatus(emails, MailQueueStatus.FAILED.toString());
            LOGGER.error(me.getMessage(), me);
        }
    }

    private static void prepareMessageBody(EmailContent emailContent,
            MimeMessageHelper helper) throws MessagingException {
        BodyPart bodyPart = new MimeBodyPart();
        bodyPart.setContent(emailContent.getEmailBody(), "text/html");
        helper.getRootMimeMultipart().addBodyPart(bodyPart);
    }

    private void prepareHeader(Email email, EmailContent emailContent,
            MimeMessageHelper helper) throws UnsupportedEncodingException,
            MessagingException {
        EmailContext context = EmailContext.getContext(emailContent
                .getEmailContext());
        helper.setSubject(emailContent.getEmailSubject());
        helper.setFrom(context.getReplyTo(), context.getDomain());
        helper.setReplyTo(context.getReplyTo(), context.getReplyTo());
        helper.setTo(email.getRecipients().toArray(new String[0]));
        helper.setCc(email.getRecipientsCc().toArray(new String[0]));
        helper.setBcc(email.getRecipientsBcc().toArray(new String[0]));
    }

    private static void prepareAttachment(List<EmailAttachment> attachments,
            MimeMessageHelper helper) throws MessagingException {
        if (attachments != null) {
            for (EmailAttachment attachment : attachments) {
                ByteArrayResource inputStream = new ByteArrayResource(
                        attachment.getAttachmentContent());
                helper.addAttachment(attachment.getFileName(), inputStream,
                        attachment.getContentType());
            }
        }
    }

    private void updateMailStatus(List<Email> emails, String status) {
        for (Email email : emails) {
            email.setMailStatus(status);
        }
        emailRepository.save(emails);
    }

    private void updateMailStatus(Email email, String status) {
        updateMailStatus(Arrays.asList(new Email[] { email }), status);
    }

    // //////////////////

    private static String getFormattedString(String template,
            VelocityContext context) {
        VelocityEngine ve = new VelocityEngine();
        // Configures the engine to use classpath to find templates
        ve.setProperty(VelocityEngine.RESOURCE_LOADER, "classpath");
        ve.setProperty(
                "classpath." + VelocityEngine.RESOURCE_LOADER + ".class",
                ClasspathResourceLoader.class.getName());
        ve.setProperty(RuntimeConstants.EVENTHANDLER_INCLUDE,
                IncludeRelativePath.class.getName());
        ve.init();
        /* next, get the Template */
        Template t = ve.getTemplate(template);
        /* now render the template into a StringWriter */
        StringWriter out = new StringWriter();
        t.merge(context, out);
        return out.toString();
    }

    private static VelocityContext getVelocityContext(
            Map<String, Object> contextMap) {
        VelocityContext cxt = new VelocityContext();
        for (Entry<String, Object> set : contextMap.entrySet()) {
            cxt.put(set.getKey(), set.getValue());
        }
        return cxt;
    }

    @Override
    public void send(EmailContext emailEvent) throws MessagingException {
        // TODO Auto-generated method stub

    }

}

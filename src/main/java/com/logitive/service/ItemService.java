package com.logitive.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logitive.LookeriaMessages;
import com.logitive.domain.Item;
import com.logitive.domain.UserItem;
import com.logitive.exception.LookeriaExceptionFactory;
import com.logitive.repository.ItemRepository;
import com.logitive.repository.UserItemRepository;
import com.logitive.service.interfaces.IItemService;

@Service
public class ItemService implements IItemService {

    private ItemRepository itemRepository;
    private UserItemRepository userItemRepository;

    @Autowired
    public ItemService(ItemRepository itemRepository,
            UserItemRepository userItemRepository) {
        super();
        this.setItemRepository(itemRepository);
        this.setUserItemRepository(userItemRepository);
    }

    public ItemRepository getItemRepository() {
        return itemRepository;
    }

    public void setItemRepository(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public UserItemRepository getUserItemRepository() {
        return userItemRepository;
    }

    public void setUserItemRepository(UserItemRepository userItemRepository) {
        this.userItemRepository = userItemRepository;
    }

    @Override
    public List<Item> findAll() {
        return itemRepository.findAll();
    }

    @Override
    public Item findById(String id) {
        return getItem(id);
    }

    @Override
    public Item save(Item item) {
        return itemRepository.save(item);
    }

    @Override
    public void delete(String id) {
        Item item = getItem(id);
        List<UserItem> userItemList = userItemRepository.findByItemId(item
                .getId());
        List<String> ids = new ArrayList<String>();
        for (UserItem userItem : userItemList) {
            ids.add(userItem.getId());
        }
        userItemRepository.removeUserItemsInIds(ids);
        itemRepository.delete(item);
    }

    @Override
    public void deleteAll() {
        itemRepository.deleteAll();
    }

    private Item getItem(String id) {
        Item item = itemRepository.findOneById(id);
        if (item == null) {
            throw LookeriaExceptionFactory.validationException(LookeriaMessages
                    .get(LookeriaMessages.ITEM_WITH_ID_NOT_FOUND, id));
        }
        return item;
    }
}

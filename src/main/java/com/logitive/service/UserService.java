package com.logitive.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logitive.LookeriaMessages;
import com.logitive.domain.Item;
import com.logitive.domain.User;
import com.logitive.domain.UserItem;
import com.logitive.exception.LookeriaExceptionFactory;
import com.logitive.repository.ItemRepository;
import com.logitive.repository.UserItemRepository;
import com.logitive.repository.UserRepository;
import com.logitive.service.interfaces.IUserService;

@Service
public class UserService implements IUserService {

    private UserRepository userRepository;
    private ItemRepository itemRepository;
    private UserItemRepository userItemRepository;

    @Autowired
    public UserService(UserRepository userRepository,
            ItemRepository itemRepository, UserItemRepository userItemRepository) {
        super();
        this.setUserRepository(userRepository);
        this.setItemRepository(itemRepository);
        this.setUserItemRepository(userItemRepository);
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public ItemRepository getItemRepository() {
        return itemRepository;
    }

    public void setItemRepository(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public UserItemRepository getUserItemRepository() {
        return userItemRepository;
    }

    public void setUserItemRepository(UserItemRepository userItemRepository) {
        this.userItemRepository = userItemRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(String id) {
        return getUser(id);
    }

    @Override
    public User save(User user) {
        // user.setPassword(new
        // BCryptPasswordEncoder().encode(user.getPassword()))
        return userRepository.save(user);
    }

    @Override
    public void delete(String id) {
        User user = getUser(id);
        userRepository.delete(user);
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public UserItem likeItem(String userId, String itemId) {
        getUser(userId);
        getItem(itemId);
        if (!userItemRepository.findByUserIdAndItemId(userId, itemId).isEmpty()) {
            throw LookeriaExceptionFactory.validationException(LookeriaMessages
                    .get(LookeriaMessages.USERITEM_ALREADY_EXISTS, userId,
                            itemId));
        }
        userItemRepository.likeItem(userId, itemId);
        return userItemRepository.findByUserIdAndItemId(userId, itemId).get(0);
    }

    @Override
    public void disLikeItem(String userId, String itemId) {
        getUser(userId);
        getItem(itemId);
        userItemRepository.dislikeItem(userId, itemId);
    }

    @Override
    public List<Item> getLikedItems(String userId) {
        getUser(userId);
        List<UserItem> likedItems = userItemRepository.findByUserId(userId);
        List<String> ids = new ArrayList<String>();
        for (UserItem userItem : likedItems) {
            ids.add(userItem.getItemId());
        }
        return itemRepository.findItemsInIds(ids);
    }

    private User getUser(String id) {
        User user = userRepository.findOneById(id);
        if (user == null) {
            throw LookeriaExceptionFactory.validationException(LookeriaMessages
                    .get(LookeriaMessages.USER_WITH_ID_NOT_FOUND, id));
        }
        return user;
    }

    private Item getItem(String id) {
        Item item = itemRepository.findOneById(id);
        if (item == null) {
            throw LookeriaExceptionFactory.validationException(LookeriaMessages
                    .get(LookeriaMessages.ITEM_WITH_ID_NOT_FOUND, id));
        }
        return item;
    }

}

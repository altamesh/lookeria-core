package com.logitive.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logitive.domain.Seo;
import com.logitive.repository.ISeoRepositoryCustom;
import com.logitive.service.interfaces.ISeoService;

@Service
public class SeoService implements ISeoService{

	private ISeoRepositoryCustom seoRepository;
	
	 @Autowired
	public SeoService(ISeoRepositoryCustom seoRepository) {
		super();
		this.seoRepository = seoRepository;
	}



	@Override
	public Seo findByUrl(String url) {
		return seoRepository.findByUrl(url);
	}

}

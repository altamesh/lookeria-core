package com.logitive.service.interfaces;

import java.util.List;

import com.logitive.domain.Item;
import com.logitive.domain.User;
import com.logitive.domain.UserItem;

public interface IUserService {

    public List<User> findAll();

    public User findById(String id);

    public User save(User user);

    public void delete(String id);

    public void deleteAll();

    public UserItem likeItem(String userId, String itemId);
    
    public void disLikeItem(String userId, String itemId);

    public List<Item> getLikedItems(String userId);

}

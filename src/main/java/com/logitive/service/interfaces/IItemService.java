package com.logitive.service.interfaces;

import java.util.List;

import com.logitive.domain.Item;

public interface IItemService {

    public List<Item> findAll();

    public Item findById(String id);

    public Item save(Item item);

    public void delete(String id);

    public void deleteAll();

}

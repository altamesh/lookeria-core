package com.logitive.service.interfaces;

import com.logitive.domain.Seo;

public interface ISeoService {

    public Seo findByUrl(String url);

}

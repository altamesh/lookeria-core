package com.logitive.service.interfaces;

import java.util.List;

import javax.mail.MessagingException;

import com.logitive.commons.email.EmailContext;
import com.logitive.commons.email.EmailDataWrapper;

public interface IEmailService {

    public void send(EmailContext emailEvent) throws MessagingException;

    public void saveAndSendBulkEmailsAsync(List<EmailDataWrapper> emailSessions);

    public void saveAndSendEmailAsync(EmailDataWrapper emailSession);
    
    public void saveAndSendBulkEmails(List<EmailDataWrapper> emailSessions);

    public void saveAndSendEmail(EmailDataWrapper emailSession);

}

package com.logitive;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.TimeZone;

public class LookeriaUtilities {

    private LookeriaUtilities() {
    }

    public static Timestamp getTimestamp() {
        return new Timestamp(Calendar.getInstance(TimeZone.getTimeZone("GMT"))
                .getTime().getTime());
    }

}

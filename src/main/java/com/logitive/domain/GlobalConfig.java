package com.logitive.domain;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "global_config")
public class GlobalConfig extends BaseEntity {

    private static final long serialVersionUID = 1L;
    
    @Field("mail_smtp_auth")
    private boolean mailSmtpAuth;
    @Field("mail_smtp_host")
	private String mailSmtpHost;
    @Field("mail_smtp_port")
	private int mailSmtpPort;
    @Field("mail_smtp_user")
	private String mailSmtpUser;
    @Field("mail_smtp_password")
	private String mailSmtpPassword;
	
//	@Enumerated(EnumType.STRING)
	private Type type = Type.LIVE;

	public enum Type {
		SANDBOX, LIVE
	}

	public boolean isMailSmtpAuth() {
		return mailSmtpAuth;
	}

	public void setMailSmtpAuth(boolean mailSmtpAuth) {
		this.mailSmtpAuth = mailSmtpAuth;
	}

	public String getMailSmtpHost() {
		return mailSmtpHost;
	}

	public void setMailSmtpHost(String mailSmtpHost) {
		this.mailSmtpHost = mailSmtpHost;
	}

	public int getMailSmtpPort() {
		return mailSmtpPort;
	}

	public void setMailSmtpPort(int mailSmtpPort) {
		this.mailSmtpPort = mailSmtpPort;
	}

	public String getMailSmtpUser() {
		return mailSmtpUser;
	}

	public void setMailSmtpUser(String mailSmtpUser) {
		this.mailSmtpUser = mailSmtpUser;
	}

	public String getMailSmtpPassword() {
		return mailSmtpPassword;
	}

	public void setMailSmtpPassword(String mailSmtpPassword) {
		this.mailSmtpPassword = mailSmtpPassword;
	}
	

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
	
}

package com.logitive.domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "item")
public class Item extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @NotEmpty
    @Field("name")
    private String name;

    @Field("total_likes")
    private Integer totalLikes;

    public Item(String name, Integer totalLikes) {
        this.name = name;
        this.totalLikes = totalLikes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(Integer totalLikes) {
        this.totalLikes = totalLikes;
    }

}

package com.logitive.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.logitive.commons.email.MailQueueStatus;

@Document(collection = "email")
public class Email extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Field("email_context")
    private String emailContext;
    @Field("sender_mail")
    private String sender;
    @NotEmpty
    @Field("recipients")
    private List<String> recipients = new ArrayList<String>();
    @Field("recipients_cc")
    private List<String> recipientsCc = new ArrayList<String>();
    @Field("recipients_bcc")
    private List<String> recipientsBcc = new ArrayList<String>();
    @Field("mail_status")
    private String mailStatus = MailQueueStatus.TO_BE_SENT.toString();
    @Field("retries")
    private Integer retries = 0;
    @Field("priority")
    private Integer priority;
    @Field("queued_at")
    private Date queuedAt;
    @Field("last_tried_at")
    private Date lastTriedAt;
    @Field("sent_at")
    private Date sentAt;
    @Field("on_hold_until")
    private Date onHoldUntil;
    @Field("email_content_id")
    private String emailContentId;
    @Field("email_attachment_ids")
    private List<String> emailAttachmentIds;

    public String getEmailContext() {
        return emailContext;
    }

    public void setEmailContext(String emailContext) {
        this.emailContext = emailContext;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public List<String> getRecipientsCc() {
        return recipientsCc;
    }

    public void setRecipientsCc(List<String> recipientsCc) {
        this.recipientsCc = recipientsCc;
    }

    public List<String> getRecipientsBcc() {
        return recipientsBcc;
    }

    public void setRecipientsBcc(List<String> recipientsBcc) {
        this.recipientsBcc = recipientsBcc;
    }

    public String getMailStatus() {
        return mailStatus;
    }

    public void setMailStatus(String mailStatus) {
        this.mailStatus = mailStatus;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Date getQueuedAt() {
        return queuedAt;
    }

    public void setQueuedAt(Date queuedAt) {
        this.queuedAt = queuedAt;
    }

    public Date getLastTriedAt() {
        return lastTriedAt;
    }

    public void setLastTriedAt(Date lastTriedAt) {
        this.lastTriedAt = lastTriedAt;
    }

    public Date getSentAt() {
        return sentAt;
    }

    public void setSentAt(Date sentAt) {
        this.sentAt = sentAt;
    }

    public Date getOnHoldUntil() {
        return onHoldUntil;
    }

    public void setOnHoldUntil(Date onHoldUntil) {
        this.onHoldUntil = onHoldUntil;
    }

    public String getEmailContentId() {
        return emailContentId;
    }

    public void setEmailContentId(String emailContentId) {
        this.emailContentId = emailContentId;
    }

    public List<String> getEmailAttachmentIds() {
        return emailAttachmentIds;
    }

    public void setEmailAttachmentIds(List<String> emailAttachmentIds) {
        this.emailAttachmentIds = emailAttachmentIds;
    }

}

package com.logitive.domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "email_attachment")
public class EmailAttachment extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Field("attachment_content")
    private byte[] attachmentContent;
    @Field("content_type")
    private String contentType;
    @NotEmpty
    @Field("file_name")
    private String fileName;
    @Field("email_id")
    private String emailId;

    public byte[] getAttachmentContent() {
        return attachmentContent;
    }

    public void setAttachmentContent(byte[] attachmentContent) {
        this.attachmentContent = attachmentContent;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

}

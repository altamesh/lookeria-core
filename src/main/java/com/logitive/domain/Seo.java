package com.logitive.domain;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "seo_url")
public class Seo {

    @Field("url")	
	private String url;
	private String fq;
	private String htmlTitle;
	private String metaDes;
	private String pageTitle;
	private Catdesc catdesc;
	private Brand brand;
	
	
	
	public Seo(String url, String fq, String htmlTitle, String metaDes, String pageTitle,Catdesc catdesc,Brand brand) {
		super();
		this.url = url;
		this.fq = fq;
		this.htmlTitle = htmlTitle;
		this.metaDes = metaDes;
		this.pageTitle = pageTitle;
		this.catdesc = catdesc;
		this.brand=brand;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFq() {
		return fq;
	}
	public void setFq(String fq) {
		this.fq = fq;
	}
	public String getHtmlTitle() {
		return htmlTitle;
	}
	public void setHtmlTitle(String htmlTitle) {
		this.htmlTitle = htmlTitle;
	}
	public String getMetaDes() {
		return metaDes;
	}
	public void setMetaDes(String metaDes) {
		this.metaDes = metaDes;
	}
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	
	public Catdesc getCatdesc() {
		return catdesc;
	}
	public void setCatdesc(Catdesc catdesc) {
		this.catdesc = catdesc;
	}
	
	public Brand getBrand() {
		return brand;
	}
	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	@Override
	public String toString() {
		return "Seo [url=" + url + ", fq=" + fq + ", htmlTitle=" + htmlTitle + ", metaDes=" + metaDes + ", pageTitle="
				+ pageTitle + "]";
	}
	
	
}

package com.logitive.domain;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "user_item")
public class UserItem extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Field("user_id")
    private String userId;

    @Field("item_id")
    private String itemId;

    public UserItem(String userId, String itemId) {
        super();
        this.userId = userId;
        this.itemId = itemId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

}

package com.logitive.dto;

import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class EmailJson {

    private String emailContext;
    private String sender;
    private List<String> recipients;
    private List<String> recipientsCc;
    private List<String> recipientsBcc;
    private String mailStatus;
    private Integer retries = 0;
    private Integer priority;
    private Timestamp queuedAt;
    private Timestamp lastTriedAt;
    private Timestamp sentAt;
    private Timestamp onHoldUntil;
    private String emailContentId;
    private List<String> emailAttachmentIds;

    public String getEmailContext() {
        return emailContext;
    }

    public void setEmailContext(String emailContext) {
        this.emailContext = emailContext;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public List<String> getRecipientsCc() {
        return recipientsCc;
    }

    public void setRecipientsCc(List<String> recipientsCc) {
        this.recipientsCc = recipientsCc;
    }

    public List<String> getRecipientsBcc() {
        return recipientsBcc;
    }

    public void setRecipientsBcc(List<String> recipientsBcc) {
        this.recipientsBcc = recipientsBcc;
    }

    public String getMailStatus() {
        return mailStatus;
    }

    public void setMailStatus(String mailStatus) {
        this.mailStatus = mailStatus;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Timestamp getQueuedAt() {
        return queuedAt;
    }

    public void setQueuedAt(Timestamp queuedAt) {
        this.queuedAt = queuedAt;
    }

    public Timestamp getLastTriedAt() {
        return lastTriedAt;
    }

    public void setLastTriedAt(Timestamp lastTriedAt) {
        this.lastTriedAt = lastTriedAt;
    }

    public Timestamp getSentAt() {
        return sentAt;
    }

    public void setSentAt(Timestamp sentAt) {
        this.sentAt = sentAt;
    }

    public Timestamp getOnHoldUntil() {
        return onHoldUntil;
    }

    public void setOnHoldUntil(Timestamp onHoldUntil) {
        this.onHoldUntil = onHoldUntil;
    }

    public String getEmailContentId() {
        return emailContentId;
    }

    public void setEmailContentId(String emailContentId) {
        this.emailContentId = emailContentId;
    }

    public List<String> getEmailAttachmentIds() {
        return emailAttachmentIds;
    }

    public void setEmailAttachmentIds(List<String> emailAttachmentIds) {
        this.emailAttachmentIds = emailAttachmentIds;
    }

}

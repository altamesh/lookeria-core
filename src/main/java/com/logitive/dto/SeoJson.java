package com.logitive.dto;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "seo_url")
public class SeoJson {

	@Field("url")
	private String url;
	private String fq;
	private String htmlTitle;
	private String metaDes;
	private String pageTitle;
	private CatdescJson catdesc;
	private BrandJson brand;

	public SeoJson(String url, String fq, String htmlTitle, String metaDes, String pageTitle, CatdescJson catdescJson,
			BrandJson brandJson) {
		super();
		this.url = url;
		this.fq = fq;
		this.htmlTitle = htmlTitle;
		this.metaDes = metaDes;
		this.pageTitle = pageTitle;
		this.catdesc = catdescJson;
		this.brand = brandJson;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFq() {
		return fq;
	}

	public void setFq(String fq) {
		this.fq = fq;
	}

	public String getHtmlTitle() {
		return htmlTitle;
	}

	public void setHtmlTitle(String htmlTitle) {
		this.htmlTitle = htmlTitle;
	}

	public String getMetaDes() {
		return metaDes;
	}

	public void setMetaDes(String metaDes) {
		this.metaDes = metaDes;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public CatdescJson getCatdesc() {
		return catdesc;
	}

	public void setCatdesc(CatdescJson catdescJson) {
		this.catdesc = catdescJson;
	}

	public BrandJson getBrand() {
		return brand;
	}

	public void setBrandJson(BrandJson brandJson) {
		this.brand = brandJson;
	}
}

package com.logitive.dto;

import java.util.ArrayList;
import java.util.List;

import com.logitive.domain.Item;

public class ItemMapper {

    private ItemMapper() {
    }

    public static ItemJson getItemJson(Item item) {
        if (item == null) {
            return null;
        }
        return new ItemJson(item.getId(), item.getName(), item.getTotalLikes());
    }

    public static Item getItem(ItemJson itemJson) {
        return new Item(itemJson.getName(), itemJson.getTotalLikes());
    }

    public static List<ItemJson> getItemJsonList(List<Item> items) {
        List<ItemJson> itemJsons = new ArrayList<ItemJson>();
        for (Item item : items) {
            itemJsons.add(getItemJson(item));
        }
        return itemJsons;
    }

}

package com.logitive.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class UserJson {

    private String id;
    private String name;
    private String email;
    private String password;
    private List<ItemJson> likedItems;

    public UserJson() {
    }

    public UserJson(String id, String name, String email, String password) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<ItemJson> getLikedItems() {
        return likedItems;
    }

    public void setLikedItems(List<ItemJson> likedItems) {
        this.likedItems = likedItems;
    }

}

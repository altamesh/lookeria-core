package com.logitive.dto;

import com.logitive.domain.UserItem;

public class UserItemMapper {

    private UserItemMapper() {
    }

    public static UserItemJson getUserItemJson(UserItem userItem) {
        if (userItem == null) {
            return null;
        }
        return new UserItemJson(userItem.getId(), userItem.getUserId(),
                userItem.getItemId());
    }

    public static UserItem getUserItem(UserItemJson userItemJson) {
        return new UserItem(userItemJson.getUserId(), userItemJson.getItemId());
    }

}

package com.logitive.dto;

import com.logitive.domain.Brand;
import com.logitive.domain.Catdesc;
import com.logitive.domain.Seo;

public class SeoMapper {

	public static SeoJson getSeoJson(Seo seo) {
		System.out.println(seo);
		if (seo == null) {
			return null;
		}
		Catdesc catdesc = seo.getCatdesc();
		Brand brand = seo.getBrand();
		return new SeoJson(seo.getUrl(), seo.getFq(), seo.getHtmlTitle(), seo.getMetaDes(), seo.getPageTitle(),
				getCatdescJson(catdesc),getBrandJson(brand));
	}

	public static Seo getSeo(SeoJson seoJson) {
		if (seoJson == null) {
			return null;
		}
		CatdescJson catdescJson = seoJson.getCatdesc();
		BrandJson brandJson = seoJson.getBrand();
		return new Seo(seoJson.getUrl(), seoJson.getFq(), seoJson.getHtmlTitle(), seoJson.getMetaDes(),
				seoJson.getPageTitle(), getCatdesc(catdescJson),getBrand(brandJson));
	}

	private static CatdescJson getCatdescJson(Catdesc catdesc) {
		if (null == catdesc) {
			return null;
		}
		return new CatdescJson(catdesc.getTitle(), catdesc.getText());
	}

	private static Catdesc getCatdesc(CatdescJson catdescJson) {
		if (null == catdescJson) {
			return null;
		}
		return new Catdesc(catdescJson.getTitle(), catdescJson.getText());
	}

	private static BrandJson getBrandJson(Brand brand) {
		if (null == brand) {
			return null;
		}
		return new BrandJson(brand.getName(),brand.getLogo());
	}

	private static Brand getBrand(BrandJson brandJson) {
		if (null == brandJson) {
			return null;
		}
		return new Brand(brandJson.getName(),brandJson.getLogo());
	}
}

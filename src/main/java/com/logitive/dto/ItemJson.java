package com.logitive.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ItemJson {

    private String id;
    private String name;
    private Integer totalLikes;

    public ItemJson() {
    }

    public ItemJson(String id, String name, Integer totalLikes) {
        this.id = id;
        this.name = name;
        this.totalLikes = totalLikes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(Integer totalLikes) {
        this.totalLikes = totalLikes;
    }

}

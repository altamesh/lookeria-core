package com.logitive.dto;

import java.util.ArrayList;
import java.util.List;

import com.logitive.domain.Item;
import com.logitive.domain.User;

public class UserMapper {

    private UserMapper() {
    }

    public static UserJson getUserJson(User user) {
        if (user == null) {
            return null;
        }
        return new UserJson(user.getId(), user.getName(), user.getEmail(),
                user.getPassword());
    }

    public static UserJson getUserJsonWithLikedItems(User user, List<Item> list) {
        if (user == null) {
            return null;
        }
        UserJson userJson = new UserJson(user.getId(), user.getName(),
                user.getEmail(), user.getPassword());
        userJson.setLikedItems(ItemMapper.getItemJsonList(list));
        return userJson;
    }

    public static User getUser(UserJson userJson) {
        return new User(userJson.getName(), userJson.getEmail(),
                userJson.getPassword());
    }

    public static List<UserJson> getUserJsonList(List<User> users) {
        List<UserJson> userJsons = new ArrayList<UserJson>();
        for (User user : users) {
            userJsons.add(getUserJson(user));
        }
        return userJsons;
    }

}

package com.logitive.exception;

public class ValidationException extends LookeriaException {

    private static final long serialVersionUID = 1L;

    public ValidationException() {
        super(Error.VALIDATION);
        this.name = ValidationException.class.getName();
    }

    public ValidationException(String message) {
        super(message, Error.VALIDATION);
        this.name = ValidationException.class.getName();
    }

    public ValidationException(Exception e) {
        super(e, Error.VALIDATION);
        this.name = ValidationException.class.getName();
    }

    public ValidationException(String message, Exception e) {
        super(message, e, Error.VALIDATION);
        this.name = ValidationException.class.getName();
    }

}

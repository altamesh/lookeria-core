package com.logitive.exception;

public class UnknownException extends LookeriaException {

    private static final long serialVersionUID = 1L;

    public UnknownException() {
        super(Error.UNKNOWN);
        this.name = UnknownException.class.getName();
    }

    public UnknownException(String message) {
        super(message, Error.UNKNOWN);
        this.name = UnknownException.class.getName();
    }

    public UnknownException(Exception e) {
        super(e, Error.UNKNOWN);
        this.name = UnknownException.class.getName();
    }

    public UnknownException(String message, Exception e) {
        super(message, e, Error.UNKNOWN);
        this.name = UnknownException.class.getName();
    }

}

package com.logitive.exception;

import java.util.Date;

import com.logitive.LookeriaMessages;

public class LookeriaExceptionFactory {

    private LookeriaExceptionFactory() {
    }

    private static LookeriaException createException(
            Class<? extends LookeriaException> clazz, String message,
            Exception exception) {
        LookeriaException lookeriaException;
        if (clazz.equals(ValidationException.class)) {
            ValidationException ve;
            if (message != null && exception != null) {
                ve = new ValidationException(message, exception);
            } else if (message != null) {
                ve = new ValidationException(message);
            } else if (exception != null) {
                ve = new ValidationException(exception);
            } else {
                ve = new ValidationException();
            }
            lookeriaException = prepareException(ve, message, exception);
        } else {
            lookeriaException = prepareException(new UnknownException(),
                    message, exception);
        }
        return lookeriaException;

    }

    private static LookeriaException prepareException(
            LookeriaException lookeriaException, String message,
            Exception exception) {
        lookeriaException.setTimestamp(new Date().getTime());
        lookeriaException.setException(exception);
        Error error = lookeriaException.getError();
        lookeriaException.setName(lookeriaException.getName());
        lookeriaException.setMessage(LookeriaMessages.get(error.getName()));
        lookeriaException.setStatus(error.getStatus());
        lookeriaException.setCode(error.getCode());
        if (message == null) {
            if (exception != null) {
                lookeriaException.setDeveloperMessage(exception.getMessage());
            } else {
                lookeriaException.setMessage(LookeriaMessages.get(error
                        .getName()));
            }
        } else {
            lookeriaException.setMessage(message);
        }
        return lookeriaException;

    }

    // ValidationException
    public static LookeriaException validationException() {
        return createException(ValidationException.class, null, null);
    }

    public static LookeriaException validationException(String message) {
        return createException(ValidationException.class, message, null);
    }

    public static LookeriaException validationException(Exception exception) {
        return createException(ValidationException.class, null, exception);
    }

    public static LookeriaException validationException(String message,
            Exception exception) {
        return createException(ValidationException.class, message, exception);
    }
}

package com.logitive.exception;

import org.springframework.http.HttpStatus;

public enum Error {

    // @formatter:off
    UNKNOWN(HttpStatus.SERVICE_UNAVAILABLE.value(), 1000, "error.unknown"),
    RECORD_NOT_FOUND(HttpStatus.UNPROCESSABLE_ENTITY.value(), 1010, "error.record.not.found"),
    VALIDATION(HttpStatus.UNPROCESSABLE_ENTITY.value(), 1020, "error.validation"),
    PRECONDITION(HttpStatus.PRECONDITION_FAILED.value(), 1030, "error.precondition"),
    CONFLICT(HttpStatus.CONFLICT.value(), 1040, "error.conflict"),
    FORBIDDEN(HttpStatus.FORBIDDEN.value(), 1050, "error.forbidden");
    // @formatter:on

    private int status;
    private int code;
    private String name;
//    private String customMessage;
//    private String developerMessage;

    private Error(int status, int code, String name
//            , String customMessage,
//            String developerMessage
            ) {
        this.status = status;
        this.code = code;
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String message) {
        this.name = message;
    }

//    public String getCustomMessage() {
//        return customMessage;
//    }
//
//    public void setCustomMessage(String customMessage) {
//        this.customMessage = customMessage;
//    }
//
//    public String getDeveloperMessage() {
//        return developerMessage;
//    }
//
//    public void setDeveloperMessage(String developerMessage) {
//        this.developerMessage = developerMessage;
//    }

}

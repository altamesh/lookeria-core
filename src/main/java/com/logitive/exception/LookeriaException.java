package com.logitive.exception;

import com.logitive.LookeriaMessages;

public class LookeriaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private int status;
    private int code;
    protected String message;
    // protected String customMessage;
    private String developerMessage;
    private long timestamp;
    protected String name;
    private Error error;
    private Exception exception;

    public LookeriaException() {
        this(LookeriaMessages.get(Error.UNKNOWN.getName()), Error.UNKNOWN);
    }
    
    public LookeriaException(String message) {
        this(message, Error.UNKNOWN);
    }

    public LookeriaException(Error e) {
        this(LookeriaMessages.get(e.getName()), e);
    }

    public LookeriaException(String message, Error e) {
        super(message);
        this.message = message;
        this.error = e;
    }

    public LookeriaException(Exception re, Error e) {
        this(re != null ? re.getMessage() : LookeriaMessages.get(e.getName()),
                re, e);
    }

    public LookeriaException(String message, Exception re, Error e) {
        super(message, re);
        this.exception = re;
        this.message = message;
        this.error = e;
    }

//    public LookeriaException(String message) {
//        super(message);
//        this.message = message;
//    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    // public String getCustomMessage() {
    // return customMessage;
    // }
    //
    // public void setCustomMessage(String customMessage) {
    // this.customMessage = customMessage;
    // }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(String developerMessage) {
        this.developerMessage = developerMessage;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

}

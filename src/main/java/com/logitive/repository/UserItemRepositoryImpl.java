package com.logitive.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.logitive.domain.Item;
import com.logitive.domain.UserItem;

public class UserItemRepositoryImpl implements UserItemRepositoryCustom {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(UserItemRepositoryImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void likeItem(String userId, String itemId) {
        UserItem userItem = new UserItem(userId, itemId);
        mongoTemplate.save(userItem);
        Item item = mongoTemplate.findById(itemId, Item.class);
        if (item.getTotalLikes() == null) {
            item.setTotalLikes(1);
        } else {
            item.setTotalLikes(item.getTotalLikes() + 1);
        }
        mongoTemplate.save(item);
        LOGGER.info("Saved UserItem with userId: " + userId + " and itemId: "
                + itemId);
    }

    @Override
    public void dislikeItem(String userId, String itemId) {
        Query query = new Query(Criteria.where("userId").is(userId)
                .and("itemId").is(itemId));
        Item item = mongoTemplate.findById(itemId, Item.class);
        if (item.getTotalLikes() != null) {
            item.setTotalLikes(item.getTotalLikes() - 1);
        }
        mongoTemplate.findAllAndRemove(query, UserItem.class);
    }

    @Override
    public void removeUserItemsInIds(List<String> userItemIds) {
        Query query = new Query(Criteria.where("id").in(userItemIds));
        mongoTemplate.remove(query, Item.class);
    }

}

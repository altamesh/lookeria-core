package com.logitive.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.logitive.commons.email.MailQueueStatus;
import com.logitive.domain.Email;

public class EmailRepositoryImpl implements EmailRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Email> findUnsentEmails() {
        Query query = new Query(Criteria.where("mailStatus").in(
                MailQueueStatus.RETRY.toString()));
        return mongoTemplate.find(query, Email.class, "email");
    }

//    @Override
//    public void setUnsentEmailStatusRetry() {
//        Query query = new Query(Criteria.where("mailStatus").nin(
//                MailQueueStatus.SENT.toString(),
//                MailQueueStatus.RETRY.toString(),
//                MailQueueStatus.FAILED.toString(),
//                MailQueueStatus.INVALID.toString()));
//        Update update = new Update();
//        update.set("mailStatus", MailQueueStatus.RETRY.toString());
//        mongoTemplate.updateMulti(query, update, Email.class);
//    }

}

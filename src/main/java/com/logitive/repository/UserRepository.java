package com.logitive.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.logitive.domain.User;

public interface UserRepository extends MongoRepository<User, String> {

    User findOneById(String id);

    User findOneByEmail(String email);

}
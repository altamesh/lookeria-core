package com.logitive.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.logitive.domain.EmailAttachment;

public interface EmailAttachmentRepository extends
        MongoRepository<EmailAttachment, String> {

    EmailAttachment findOneById(String id);

}
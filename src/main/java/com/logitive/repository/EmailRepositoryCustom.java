package com.logitive.repository;

import java.util.List;

import com.logitive.domain.Email;

public interface EmailRepositoryCustom {

    List<Email> findUnsentEmails();

}

package com.logitive.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.logitive.domain.Item;

public class ItemRepositoryImpl implements ItemRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Item> findItemsInIds(List<String> itemIds) {
        Query query = new Query(Criteria.where("id").in(itemIds));
        return mongoTemplate.find(query, Item.class);
    }

}

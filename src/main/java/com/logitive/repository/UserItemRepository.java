package com.logitive.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.logitive.domain.UserItem;

public interface UserItemRepository extends MongoRepository<UserItem, String>,
        UserItemRepositoryCustom {

    UserItem findOneById(String id);

    List<UserItem> findByUserIdAndItemId(String userId, String itemId);
    
    List<UserItem> findByUserId(String userId);
    
    List<UserItem> findByItemId(String itemId);

}
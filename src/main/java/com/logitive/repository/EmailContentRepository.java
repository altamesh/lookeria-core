package com.logitive.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.logitive.domain.EmailContent;

public interface EmailContentRepository extends
        MongoRepository<EmailContent, String> {

    EmailContent findOneById(String id);

}
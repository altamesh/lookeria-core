package com.logitive.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.logitive.domain.Seo;

public interface ISeoRepositoryCustom extends MongoRepository<Seo, String> {

	Seo findByUrl(String id);
}
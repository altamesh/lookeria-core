package com.logitive.repository;

import java.util.List;

import com.logitive.domain.Item;

public interface ItemRepositoryCustom {

    List<Item> findItemsInIds(List<String> itemIds);

}

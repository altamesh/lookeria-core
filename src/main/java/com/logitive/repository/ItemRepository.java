package com.logitive.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.logitive.domain.Item;

public interface ItemRepository extends MongoRepository<Item, String>,
        ItemRepositoryCustom {

    Item findOneById(String id);

}
package com.logitive.repository;

import com.logitive.domain.GlobalConfig;

public interface GlobalConfigRepositoryCustom {

    GlobalConfig getFirst();

}

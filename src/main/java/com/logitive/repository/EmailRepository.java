package com.logitive.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.logitive.domain.Email;

public interface EmailRepository extends MongoRepository<Email, String>, EmailRepositoryCustom {

    Email findOneById(String id);

}
package com.logitive.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.logitive.domain.GlobalConfig;
import com.logitive.domain.Item;

public interface GlobalConfigRepository extends
        MongoRepository<GlobalConfig, String>, GlobalConfigRepositoryCustom {

    Item findOneById(String id);

}
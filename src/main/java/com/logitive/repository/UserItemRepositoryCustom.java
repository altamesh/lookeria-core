package com.logitive.repository;

import java.util.List;

public interface UserItemRepositoryCustom {

    void likeItem(String userId, String itemId);
    
    void dislikeItem(String userId, String itemId);

    void removeUserItemsInIds(List<String> userItemIds);

}

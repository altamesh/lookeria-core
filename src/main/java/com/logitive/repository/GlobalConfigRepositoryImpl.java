package com.logitive.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.logitive.domain.GlobalConfig;

public class GlobalConfigRepositoryImpl implements GlobalConfigRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public GlobalConfig getFirst() {
        List<GlobalConfig> configs = mongoTemplate.findAll(GlobalConfig.class);
        if (!configs.isEmpty()) {
            return configs.get(0);
        }
        return null;
    }

}

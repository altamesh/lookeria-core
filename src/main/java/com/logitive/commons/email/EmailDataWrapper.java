package com.logitive.commons.email;

import java.util.List;

import com.logitive.domain.Email;
import com.logitive.domain.EmailAttachment;
import com.logitive.domain.EmailContent;

public class EmailDataWrapper {

    EmailContext context;
    Email email;
    // emailContent usually comes from template, but could be set manually.
    EmailContent emailContent;
//    Map<String, Object> emailAttributes;
    List<EmailAttachment> attachments;

    public EmailDataWrapper(EmailContext context, Email email,
            EmailContent emailContent, 
//            Map<String, Object> emailAttributes,
            List<EmailAttachment> attachments) {
        this.context = context;
        this.email = email;
        this.emailContent = emailContent;
//        this.emailAttributes = emailAttributes;
        this.attachments = attachments;
    }

    public EmailContext getContext() {
        return context;
    }

    public void setContext(EmailContext context) {
        this.context = context;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public EmailContent getEmailContent() {
        return emailContent;
    }

    public void setEmailContent(EmailContent emailContent) {
        this.emailContent = emailContent;
    }

//    public Map<String, Object> getEmailAttributes() {
//        return emailAttributes;
//    }

//    public void setEmailAttributes(Map<String, Object> emailAttributes) {
//        this.emailAttributes = emailAttributes;
//    }

    public List<EmailAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<EmailAttachment> attachments) {
        this.attachments = attachments;
    }

}

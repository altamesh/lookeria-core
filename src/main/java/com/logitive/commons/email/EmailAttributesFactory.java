package com.logitive.commons.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.logitive.domain.Email;
import com.logitive.domain.User;
import com.logitive.repository.UserRepository;

@Component
public class EmailAttributesFactory {

    @Autowired
    private UserRepository userRepository;

    public Map<String, Object> getAttributes(EmailDataWrapper emailSession) {
        EmailContext context = emailSession.getContext();
        Map<String, Object> emailAttributes = new HashMap<>();
        if (context == null) {
            return emailAttributes;
        }
        if (context == EmailContext.TEST) {
            Email email = emailSession.getEmail();
            User recipient = userRepository.findOneByEmail(email
                    .getRecipients().get(0));
            if (recipient != null) {
                emailAttributes.put("recipient", recipient.getName());
            }
            emailAttributes.put("sender", email.getSender());
        }
        return emailAttributes;
    }
}

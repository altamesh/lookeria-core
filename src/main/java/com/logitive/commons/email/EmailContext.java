package com.logitive.commons.email;

public enum EmailContext {
    // @formatter:off
	TEST(
	        "test",
			"no-reply@lookeria.com",
			"Test mail from Lookeria",
			"Lookeria.com",
			"com/logitive/lookeria/shared/template/email/velocity/test-template.vm");
	// @formatter:on

    private String name;
    private String replyTo;
    private String subject;
    private String domain;
    private String template;

    private EmailContext(String name, String replyTo, String subject,
            String domain, String template) {
        this.name = name;
        this.replyTo = replyTo;
        this.subject = subject;
        this.domain = domain;
        this.template = template;
    }

    public static EmailContext getContext(String name) {
        for (EmailContext emailContext : values()) {
            if (emailContext.getName().equalsIgnoreCase(name)) {
                return emailContext;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public String getSubject() {
        return subject;
    }

    public String getDomain() {
        return domain;
    }

    public String getTemplate() {
        return template;
    }

}

package com.logitive.commons.email;

public enum MailQueueStatus {
    
    /** new email to be sent */
    TO_BE_SENT,
    /** email sent successfully */
    SENT,
    /** sending of email has failed and should be retried */
    RETRY,
    /** sending of email has failed finally */
    FAILED,
    /** sending email is in progress */
    PROGRESS,
    /** email sending has been stopped */
    STOPPED,
    /** content is invalid */
    INVALID;
    
}

package com.logitive.commons.scheduledtasks;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.logitive.commons.email.EmailContext;
import com.logitive.commons.email.EmailDataWrapper;
import com.logitive.domain.Email;
import com.logitive.repository.EmailRepository;
import com.logitive.service.EmailService;
import com.logitive.service.interfaces.IEmailService;

@Component
public class ScheduledTasks {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(EmailService.class.getName());
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private IEmailService emailService;

    @Autowired
    private EmailRepository emailRepository;

    @Scheduled(cron = "${pending.email.sending.interval}")
    public void sendPendingEmail() {
        List<Email> pendingEmails = emailRepository.findUnsentEmails();
        if (!pendingEmails.isEmpty()) {
            LOGGER.info("Starting sending of pending emails at "
                    + dateFormat.format(new Date()));
            List<EmailDataWrapper> emailSessions = new ArrayList<>();
            for (Email email : pendingEmails) {
                EmailContext context = EmailContext.getContext(email
                        .getEmailContext());
                emailSessions.add(new EmailDataWrapper(context, email, null,
                        null));
            }
            emailService.saveAndSendBulkEmails(emailSessions);
            LOGGER.info("Finished sending of pending emails at "
                    + dateFormat.format(new Date()));
        }
    }
}
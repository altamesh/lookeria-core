package com.logitive;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import org.springframework.context.i18n.LocaleContextHolder;

public class LookeriaMessages {

    private static final ResourceBundle resourceBundle = ResourceBundle
            .getBundle("com.logitive.messages.message", LocaleContextHolder.getLocale());

    // Errors
    public static final String ERROR_UNKNOWN = "error.unknown";
    public static final String ERROR_RECORD_NOT_FOUND = "error.record.not.found";
    public static final String ERROR_VALIDATION = "error.validation";
    public static final String ERROR_PRECONDITION = "error.precondition";
    public static final String ERROR_CONFLICT = "error.conflict";
    public static final String ERROR_FORBIDDEN = "error.forbidden";
    // Service messages
    public static final String ITEM_WITH_ID_NOT_FOUND = "item.with.id.not.found";
    public static final String USER_WITH_ID_NOT_FOUND = "user.with.id.not.found";
    public static final String USERITEM_ALREADY_EXISTS = "useritem.already.exists";

    private LookeriaMessages() {
    }

    public static String get(String key) {
        return resourceBundle.getString(key);
    }

    public static String get(String key, Object... param) {
        return MessageFormat.format(get(key), param);
    }
}

package com.logitive.controller.v1;

import org.springframework.web.bind.annotation.RequestMapping;

import com.logitive.service.interfaces.IItemService;
import com.logitive.service.interfaces.ISeoService;
import com.logitive.service.interfaces.IUserService;

@RequestMapping("/v1")
public class AbstractController {

    protected IUserService userService;
    protected IItemService itemService;
    protected ISeoService seoService;

    public IUserService getUserService() {
        return userService;
    }

    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    public IItemService getItemService() {
        return itemService;
    }

    public void setItemService(IItemService itemService) {
        this.itemService = itemService;
    }

	public ISeoService getSeoService() {
		return seoService;
	}

	public void setSeoService(ISeoService seoService) {
		this.seoService = seoService;
	}
    
}

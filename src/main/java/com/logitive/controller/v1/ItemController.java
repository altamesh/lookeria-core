package com.logitive.controller.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.logitive.domain.Item;
import com.logitive.dto.ItemJson;
import com.logitive.dto.ItemMapper;
import com.logitive.service.interfaces.IItemService;

@RestController
public class ItemController extends AbstractController {

    @Autowired
    public ItemController(IItemService itemService) {
        super();
        this.setItemService(itemService);
    }

    @RequestMapping(value = "/items", method = RequestMethod.POST)
    public ResponseEntity<ItemJson> saveItem(@RequestBody ItemJson itemJson) {
        Item item = ItemMapper.getItem(itemJson);
        item = itemService.save(item);
        return new ResponseEntity<ItemJson>(ItemMapper.getItemJson(item),
                HttpStatus.CREATED);
    }

    @RequestMapping(value = "/items/{id}", method = RequestMethod.GET)
    public ResponseEntity<ItemJson> getItem(@PathVariable String id) {
        return new ResponseEntity<ItemJson>(ItemMapper.getItemJson(itemService
                .findById(id)), HttpStatus.OK);
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public List<ItemJson> getAllItems() {
        return ItemMapper.getItemJsonList(itemService.findAll());
    }

    @RequestMapping(value = "/items/{id}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable String id) {
        itemService.delete(id);
    }

    @RequestMapping(value = "/items", method = RequestMethod.DELETE)
    public void deleteAllItems() {
        itemService.deleteAll();
    }

}

package com.logitive.controller.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.logitive.domain.Item;
import com.logitive.domain.User;
import com.logitive.domain.UserItem;
import com.logitive.dto.UserItemJson;
import com.logitive.dto.UserItemMapper;
import com.logitive.dto.UserJson;
import com.logitive.dto.UserMapper;
import com.logitive.service.interfaces.IUserService;

@RestController
public class UserController extends AbstractController {

    @Autowired
    public UserController(IUserService userService) {
        super();
        this.setUserService(userService);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<UserJson> saveUser(@RequestBody UserJson userJson) {
        User user = UserMapper.getUser(userJson);
        user = userService.save(user);
        return new ResponseEntity<UserJson>(UserMapper.getUserJson(user),
                HttpStatus.CREATED);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserJson> getUser(@PathVariable String id) {
        return new ResponseEntity<UserJson>(UserMapper.getUserJson(userService
                .findById(id)), HttpStatus.OK);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserJson> getAllUsers() {
        return UserMapper.getUserJsonList(userService.findAll());
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable String id) {
        userService.delete(id);
    }

    @RequestMapping(value = "/users", method = RequestMethod.DELETE)
    public void deleteAllUsers() {
        userService.deleteAll();
    }

    @RequestMapping(value = "/users/{userId}/{itemId}/like", method = RequestMethod.GET)
    public UserItemJson likeItem(@PathVariable String userId,
            @PathVariable String itemId) {
        UserItem userItem = userService.likeItem(userId, itemId);
        return UserItemMapper.getUserItemJson(userItem);
    }

    @RequestMapping(value = "/users/{userId}/{itemId}/dislike", method = RequestMethod.GET)
    public void disLikeItem(@PathVariable String userId,
            @PathVariable String itemId) {
        userService.disLikeItem(userId, itemId);
    }

    @RequestMapping(value = "/users/{userId}/liked", method = RequestMethod.GET)
    public UserJson getLikedItems(@PathVariable String userId) {
        User user = userService.findById(userId);
        List<Item> userItemList = userService.getLikedItems(userId);
        return UserMapper.getUserJsonWithLikedItems(user, userItemList);
    }

}

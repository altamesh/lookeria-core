package com.logitive.controller.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.logitive.domain.Seo;
import com.logitive.dto.SeoJson;
import com.logitive.dto.SeoMapper;
import com.logitive.service.interfaces.ISeoService;

@CrossOrigin
@RestController
public class SeoController extends AbstractController {

    @Autowired
    public SeoController(ISeoService seoService) {
        super();
        this.setSeoService(seoService);
    }

    @RequestMapping(value = "/seo/{url}", method = RequestMethod.GET)
    public ResponseEntity<SeoJson> getSeoUrl(@PathVariable String url) {
    	Seo seo=seoService
        .findByUrl(url);
    	if(seo==null){
            return new ResponseEntity<SeoJson>(HttpStatus.NOT_FOUND);
    	}else{
            return new ResponseEntity<SeoJson>(SeoMapper.getSeoJson(seo), HttpStatus.OK);

    	}
    }

}

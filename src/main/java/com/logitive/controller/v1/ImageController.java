package com.logitive.controller.v1;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logitive.service.interfaces.ISeoService;
import com.mortennobel.imagescaling.AdvancedResizeOp;
import com.mortennobel.imagescaling.MultiStepRescaleOp;

import javafx.util.Pair;

@CrossOrigin
@RestController
public class ImageController extends AbstractController {
    private final static Logger LOGGER = Logger.getLogger(ImageController.class.getName());

    @Autowired
    public ImageController(ISeoService seoService) {
        super();
        this.setSeoService(seoService);
    }

    @RequestMapping(value = "/rsz", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getSeoUrl(@RequestParam("url") String url,
            @RequestParam("w") String w, @RequestParam("h") String h, HttpServletResponse response)
            throws IOException {

        checkParams(url, w, h);
        InputStream input = new URL(url).openStream();
        ImageInputStream iis = ImageIO.createImageInputStream(input);
        Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
        String format = "JPEG";
        if (iter.hasNext()) {
            format = iter.next().getFormatName();
            response.setContentType("image/" + format);
        } else {
            response.setContentType("image/jpeg");
        }

        try {
            byte[] imageInByte;
            // Case 1
            BufferedImage originalImage = ImageIO.read(new URL(url).openStream());
            // Get width width and height
            Pair<Integer, Integer> widthHeight = getWidthHeightPair(originalImage, w, h);
            MultiStepRescaleOp rescale = new MultiStepRescaleOp(widthHeight.getKey(),
                    widthHeight.getValue());
            rescale.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Soft);
            BufferedImage resizedImage = rescale.filter(originalImage, null);
            // convert BufferedImage to byte array
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(resizedImage, format, baos);
            baos.flush();
            imageInByte = baos.toByteArray();
            baos.close();
            originalImage.flush();
            resizedImage.flush();
            input.close();
            iis.close();

            final HttpHeaders headers = new HttpHeaders();
            if ("GIF".equalsIgnoreCase(format)) {
                headers.setContentType(MediaType.IMAGE_GIF);
            } else if ("JPEG".equalsIgnoreCase(format)) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if ("PNG".equalsIgnoreCase(format)) {
                headers.setContentType(MediaType.IMAGE_PNG);
            }

            return new ResponseEntity<>(imageInByte, headers, HttpStatus.CREATED);
        } catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, ioe.getLocalizedMessage(), ioe);
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    private Pair<Integer, Integer> getWidthHeightPair(BufferedImage originalImage, String wString,
            String hString) {
        int originalWidth = originalImage.getWidth();
        int originalHeight = originalImage.getHeight();
        int w;
        int h;
        double aspectRatio = originalWidth / (double) originalHeight;
        int requiredWidth = Integer.parseInt(wString);
        int requiredHeight = Integer.parseInt(hString);
        boolean accomodateWidth = (originalWidth - originalHeight) >= 0;
        if (accomodateWidth) {
            w = requiredWidth;
            h = (int) (w / aspectRatio);
            if (h > requiredHeight) {
                h = requiredHeight;
                w = (int) (aspectRatio * h);
            }
        } else {
            h = requiredHeight;
            w = (int) (aspectRatio * h);
            if (w > requiredWidth) {
                w = requiredWidth;
                h = (int) (w / aspectRatio);
            }
        }
        return new Pair<>(w, h);
    }

    private void checkParams(String url, String wString, String hString) {
        if (url == null) {
            throw new RuntimeException("Query param url is required");
        }
        if (wString == null) {
            throw new RuntimeException("Query param w is required");
        }
        if (hString == null) {
            throw new RuntimeException("Query param w is required");
        }
        if (!StringUtils.isNumeric(wString)) {
            throw new RuntimeException("Query param w is non numeric");
        }
        if (!StringUtils.isNumeric(hString)) {
            throw new RuntimeException("Query param h is non numeric");
        }
    }

}

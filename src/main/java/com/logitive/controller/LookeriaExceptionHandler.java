package com.logitive.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logitive.exception.LookeriaException;

//@EnableWebMvc
@ControllerAdvice
public class LookeriaExceptionHandler {

    @ExceptionHandler({ LookeriaException.class })
//    @ResponseBody
    protected ModelAndView handleInvalidRequest(LookeriaException e,
            HttpServletResponse response) {
        ModelAndView mv = new ModelAndView();
        response.setStatus(e.getStatus());
        MappingJackson2JsonView view = new MappingJackson2JsonView();
        view.setObjectMapper(new ObjectMapper());
        mv.addObject("timestamp", e.getTimestamp());
        if (e.getStatus() != 0) {
            mv.addObject("status", e.getStatus());
        }
        if (e.getCode() != 0) {
            mv.addObject("code", e.getCode());
        }
        if (e.getName() != null) {
            mv.addObject("name", e.getName());
        }
        if (e.getMessage() != null) {
            mv.addObject("message", e.getMessage());
        }
        if (e.getDeveloperMessage() != null) {
            mv.addObject("developerMessage", e.getDeveloperMessage());
        }
        mv.setView(view);
        return mv;
    }
}

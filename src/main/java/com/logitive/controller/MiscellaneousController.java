package com.logitive.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logitive.commons.email.EmailContext;
import com.logitive.commons.email.EmailDataWrapper;
import com.logitive.domain.Email;
import com.logitive.domain.User;
import com.logitive.dto.EmailJson;
import com.logitive.repository.UserRepository;
import com.logitive.service.interfaces.IEmailService;

@RestController
public class MiscellaneousController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private IEmailService emailService;

    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
    public String greet(
            @RequestParam(value = "name", defaultValue = "World") String name) {
        Principal principal = request.getUserPrincipal();
        if (principal != null) {
            User user = userRepository.findOneByEmail(principal.getName());
            if (user != null) {
                name = user.getName();
            }
        }
        return String.format("Hello, %s!", name);
    }

    @RequestMapping(value = "/sendemail", method = RequestMethod.POST)
    public void sendEmail(@RequestBody EmailJson emailJson) {
        EmailContext context = EmailContext.getContext(emailJson
                .getEmailContext());
        Email email = new Email();
        email.setEmailContext(context.toString());
        email.setRecipients(emailJson.getRecipients());
        email.setSender(emailJson.getSender());
        EmailDataWrapper emailSession = new EmailDataWrapper(context, email,
                null, null);
        emailService.saveAndSendEmailAsync(emailSession);
    }

}
